class E {
  map = {}
  on(eventName, method) {
    this.map = {
      ...this.map,
      [eventName]: [...(this.map[eventName] || []), method],
    }
  }
  emit(eventName, ...args) {
    ;(this.map[eventName] || []).forEach(method => {
      method(...args)
    })
  }
}

export {E}
export default E
