import E from './E'

const e = new E()

e.on('sum', (n1, n2) => console.log(n1 + n2))
e.on('sum', (n1, n2) => console.log(n1 * n2))

e.emit('sum', 1, 2)
